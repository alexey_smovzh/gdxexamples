#ifdef GL_ES
    #define LOWP lowp
    #define MED mediump
    #define HIGH highp
    precision mediump float;
#else
    #define MED
    #define LOWP
    #define HIGH
#endif

uniform mat4 u_worldTrans;
uniform mat4 u_projViewTrans;
uniform float u_time;

attribute vec2 a_texCoord0;

varying vec2 v_normalUV;
varying vec2 v_rotateUV;
//varying vec4 v_light;


mat2 rotation(float a) {
    float s = sin(a);
    float c = cos(a);

    return mat2(c, -s, s, c);
}

// TODO: https://github.com/mattdesl/lwjgl-basics/wiki/ShaderLesson6
void main() {
    // Calc texture coord
    v_normalUV = a_texCoord0;
    v_rotateUV = a_texCoord0;
    v_rotateUV = (v_rotateUV - 0.5) * rotation(u_time * 0.1);
    v_rotateUV += 0.5;

    // Calc light
//    v_light = phong(vec3(0.0, 10.0, 20.0));     // Light position

    gl_Position = u_projViewTrans * u_worldTrans * vec4(a_position, 1.0);

}
