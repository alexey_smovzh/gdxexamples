#ifdef GL_ES
	#define LOWP lowp
	#define MED mediump
	#define HIGH highp
	precision mediump float;
#else
	#define MED
	#define LOWP
	#define HIGH
#endif

#define UV_TEXTURE vec2(2.0, 1.0)

uniform mat4 u_worldTrans;
uniform mat4 u_projViewTrans;
uniform float u_time;
uniform vec2 u_uvCoord;
uniform float u_particleStartTime;
uniform vec3 u_particleVelocity;

attribute vec3 a_position;
attribute vec2 a_texCoord0;

varying vec2 v_diffuseUV;

void main() {

    v_diffuseUV = (a_texCoord0 / UV_TEXTURE) + u_uvCoord;                       // Calc texture coord
    float elapsedTime = u_time - u_particleStartTime;                           // Calc time
    vec3 currentPosition = a_position + (u_particleVelocity * elapsedTime);     // Calc position

    gl_Position = u_projViewTrans * u_worldTrans * vec4(currentPosition, 1.0);

}