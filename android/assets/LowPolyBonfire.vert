#ifdef GL_ES
    #define LOWP lowp
    #define MED mediump
    #define HIGH highp
    precision mediump float;
#else
    #define MED
    #define LOWP
    #define HIGH
#endif

uniform mat4 u_worldTrans;
uniform mat4 u_projViewTrans;
uniform float u_time;

varying vec4 v_light;

float rand(vec2 co) {

    float a = 12.9898;
    float b = 78.233;
    float c = 43758.5453;
    float dt= dot(co.xy ,vec2(a,b));
    float sn= mod(dt,3.14);

    return fract(sin(sn) * c);
}

float rand(float z) {

    float a = 12.9898;
    float b = 78.233;
    float c = 43758.5453;
    float dt= dot(z, b);
    float sn= mod(dt,3.14);

    return fract(sin(sn) * c);
}

void main() {

    v_light = phong(vec3(0.0, 10.0, 20.0));     // Light position

    vec4 position = vec4(a_position, 1.0);
    position.z += sin(position.x + u_time) * cos(position.y + u_time) * 2.0;

//    gl_Position = u_projViewTrans * u_worldTrans * vec4(a_position, 1.0);
    gl_Position = u_projViewTrans * u_worldTrans * position;

}
