uniform mat4 u_projViewTrans;
uniform mat4 u_worldTrans;
uniform mat3 u_normalMatrix;

uniform vec3 u_lightPos;

attribute vec3 a_position;
attribute vec3 a_normal;

varying float v_intensity;

//const vec3 Color0 = vec3(0.117, 0.371, 0.578);
//const vec3 Color1 = vec3(0.01, 0.04, 0.074);


void main() {

    vec3 transNorm = normalize(u_normalMatrix * a_normal);
    vec3 eyePos = vec3(u_projViewTrans * vec4(a_position, 1.0)) * a_position;

    v_intensity = dot(normalize(u_lightPos - eyePos), transNorm);
    v_intensity = abs(v_intensity);

    gl_Position = u_projViewTrans * u_worldTrans * vec4(a_position, 1.0);
}


