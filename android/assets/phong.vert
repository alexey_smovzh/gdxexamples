#ifdef GL_ES
    #define LOWP lowp
    #define MED mediump
    #define HIGH highp
    precision mediump float;
#else
    #define MED
    #define LOWP
    #define HIGH
#endif


uniform mat3 u_normalMatrix;

attribute vec3 a_normal;
attribute vec3 a_position;

const vec3 u_lightPos = vec3(0.0, 10.0, 20.0);
const vec3 matReflect = vec3(1, 1, 1);
const vec3 u_ambientLight = vec3(1.0, 1.0, 1.0);
const vec3 u_diffuseLight = vec3(1.0, 1.0, 1.0);


vec4 phong(vec3 L) {

    vec3 N = normalize(u_normalMatrix * a_normal);
    vec3 D = normalize(L - a_position);
    vec3 Iamb = matReflect * u_ambientLight;
    float diffuseTerm = clamp(dot(N, D), 0, 1) ;
    vec3 Idif = matReflect * u_diffuseLight * diffuseTerm;

    return vec4((Iamb + Idif), 1.0);

}

