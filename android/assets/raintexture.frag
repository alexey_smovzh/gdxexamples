#ifdef GL_ES
	#define LOWP lowp
	#define MED mediump
	#define HIGH highp
	precision mediump float;
#else
	#define MED
	#define LOWP
	#define HIGH
#endif

#define DARKNESS vec4(0.0, 0.0, 0.0, 0.4)


uniform sampler2D u_diffuseTexture;
uniform float u_factor;

varying MED vec2 v_diffuseUV;

void main() {

    vec4 diffuse = texture2D(u_diffuseTexture, v_diffuseUV);
    vec4 dark = DARKNESS * (1.0 - u_factor);

    if(diffuse.a <= u_factor) {
        gl_FragColor = dark;
    } else {
        gl_FragColor = diffuse * dark;
    }

}