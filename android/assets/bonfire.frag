#ifdef GL_ES
	#define LOWP lowp
	#define MED mediump
	#define HIGH highp
	precision mediump float;
#else
	#define MED
	#define LOWP
	#define HIGH
#endif


#define START_COLOR vec4(1.0, 1.0, 0.4, 0.8)
#define END_COLOR vec4(1.0, 0.0, 0.0, 0.0)

uniform sampler2D u_diffuseTexture;

varying float v_elapsedTime;
varying MED vec2 v_diffuseUV;

void main() {
    vec4 diffuse = texture2D(u_diffuseTexture, v_diffuseUV);

    if(diffuse.a <= 0.01) discard;

    vec4 color = mix(START_COLOR, END_COLOR, v_elapsedTime / 3.0);

    gl_FragColor = color * diffuse;
}