#ifdef GL_ES
    #define LOWP lowp
    #define MED mediump
    #define HIGH highp
    precision mediump float;
#else
    #define MED
    #define LOWP
    #define HIGH
#endif

uniform vec4 u_diffuseColor;

varying vec4 v_light;

void main() {
	gl_FragColor = u_diffuseColor * v_light;
}
