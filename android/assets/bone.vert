#define NUM_BONES 3

uniform mat4 u_worldTrans;
uniform mat4 u_projViewTrans;

uniform mat4 u_bones[NUM_BONES];

attribute vec2 a_boneWeight0;
attribute vec2 a_boneWeight1;
attribute vec2 a_boneWeight2;

varying vec4 v_light;

void main() {

    v_light = phong(vec3(0.0, 10.0, 20.0));     // Light position

    mat4 skinning = mat4(0.0);
    skinning += (a_boneWeight0.y) * u_bones[int(a_boneWeight0.x)];
    skinning += (a_boneWeight1.y) * u_bones[int(a_boneWeight1.x)];
    skinning += (a_boneWeight2.y) * u_bones[int(a_boneWeight2.x)];

	gl_Position = u_projViewTrans * u_worldTrans * skinning * vec4(a_position, 1.0);

}