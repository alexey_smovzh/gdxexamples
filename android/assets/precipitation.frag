#ifdef GL_ES
	#define LOWP lowp
	#define MED mediump
	#define HIGH highp
	precision mediump float;
#else
	#define MED
	#define LOWP
	#define HIGH
#endif

uniform sampler2D u_diffuseTexture;

varying MED vec2 v_diffuseUV;

void main() {

    vec4 diffuse = texture2D(u_diffuseTexture, v_diffuseUV);

    if(diffuse.a <= 0.01) discard;

    gl_FragColor = diffuse;

}