#ifdef GL_ES
    #define LOWP lowp
    #define MED mediump
    #define HIGH highp
    precision mediump float;
#else
    #define MED
    #define LOWP
    #define HIGH
#endif


uniform mat4 u_worldTrans;
uniform mat4 u_projViewTrans;
uniform mat3 u_normalMatrix;

attribute vec3 a_position;
attribute vec3 a_normal;

varying vec4 v_light;

const vec3 u_lightPos = vec3(0.0, 10.0, 20.0);
const vec3 matReflect = vec3(1, 1, 1);
const vec3 u_ambientLight = vec3(1.0, 1.0, 1.0);
const vec3 u_diffuseLight = vec3(1.0, 1.0, 1.0);


void main() {

    // Light http://sunandblackcat.com/tipFullView.php?l=eng&topicid=30&topic=Phong-Lighting
    vec3 N = normalize(u_normalMatrix * a_normal);
    vec3 L = normalize(u_lightPos - a_position);

    vec3 Iamb = matReflect * u_ambientLight;
    float diffuseTerm = clamp(dot(N, L), 0, 1);
    vec3 Idif = matReflect * u_diffuseLight * diffuseTerm;

    v_light = vec4((Iamb + Idif), 1.0);


    gl_Position = u_projViewTrans * u_worldTrans * vec4(a_position, 1.0);
}




