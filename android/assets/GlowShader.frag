#ifdef GL_ES
	#define LOWP lowp
	#define MED mediump
	#define HIGH highp
	precision mediump float;
#else
	#define MED
	#define LOWP
	#define HIGH
#endif


varying float v_intensity;


const vec3 Color0 = vec3(0.117, 0.371, 0.578);
const vec3 Color1 = vec3(0.01, 0.04, 0.074);


void main() {

    gl_FragColor = vec4(v_intensity * Color0, 1.0);

}

