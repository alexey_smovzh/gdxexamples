#ifdef GL_ES
	#define LOWP lowp
	#define MED mediump
	#define HIGH highp
	precision mediump float;
#else
	#define MED
	#define LOWP
	#define HIGH
#endif

uniform mat4 u_worldTrans;
uniform mat4 u_projViewTrans;
uniform float u_time;
uniform vec2 u_velocity;

attribute vec3 a_position;
attribute vec2 a_texCoord0;

varying vec2 v_diffuseUV;


void main() {

    v_diffuseUV = a_texCoord0 + (u_velocity * u_time);

    gl_Position = u_projViewTrans * u_worldTrans * vec4(a_position, 1.0);

}