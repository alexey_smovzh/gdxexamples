#ifdef GL_ES
    #define LOWP lowp
    #define MED mediump
    #define HIGH highp
    precision mediump float;
#else
    #define MED
    #define LOWP
    #define HIGH
#endif

uniform sampler2D u_diffuseTexture;
uniform sampler2D u_normalTexture;
uniform vec2 u_resolution;

varying vec2 v_normalUV;
varying vec2 v_rotateUV;
//varying vec4 v_light;

const vec3 light = vec3(1.0);
const vec3 dir = vec3(-0.2595907,-0.30534133,0.91617787);   // light direction

const vec3 dColor0 = vec3(0.1, 0.2, 0.3);
const vec3 dColor1 = vec3(.5, .6, .7);
const vec3 nColor0 = vec3(.004, .016, .046);
const vec3 nColor1 = vec3(.005, .102, .334);


void main() {

    // Gradient
    vec2 position = gl_FragCoord.xy / u_resolution.xy;
    vec3 gradient = vec3(mix(dColor0, nColor1, position.y));

    //
	vec4 diffuse = texture2D(u_diffuseTexture, v_rotateUV);
	vec4 normal = texture2D(u_normalTexture, v_normalUV);

    // Mix gradient and diffuse texture
    vec3 color = vec3(mix(gradient, diffuse.rgb, diffuse.a));

    // Calculates normal
    vec3 n = normalize(normal.xyz * 2.0 - 1.0);             // normal
    vec3 l = normalize(dir);
    vec3 c = light * max(dot(n, l), 0.0);

//    gl_FragColor = diffuse;
    gl_FragColor = vec4(color * c, 1.0);
//    gl_FragColor = vec4(color, 1.0) * v_light;

}
