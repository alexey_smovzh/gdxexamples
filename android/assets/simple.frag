#ifdef GL_ES
	#define LOWP lowp
	#define MED mediump
	#define HIGH highp
	precision mediump float;
#else
	#define MED
	#define LOWP
	#define HIGH
#endif

uniform sampler2D u_normal0;
uniform sampler2D u_normal1;
uniform float u_factor;

varying vec2 v_uv;

void main() {

    vec4 n0 = texture2D(u_normal0, v_uv);
    vec4 n1 = texture2D(u_normal1, v_uv);

    gl_FragColor = mix(n0, n1, u_factor);
}