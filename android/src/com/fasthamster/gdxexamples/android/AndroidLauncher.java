package com.fasthamster.gdxexamples.android;

import android.os.Bundle;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.fasthamster.gdxexamples.ReloadTextureToShader;

public class AndroidLauncher extends AndroidApplication {
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
//		initialize(new gdxExamples(), config);
//		initialize(new Bonfire(), config);
//		initialize(new Precipitation(), config);
//		initialize(new RainTexture(), config);
		initialize(new ReloadTextureToShader(), config);

	}
}
