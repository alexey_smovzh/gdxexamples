package com.fasthamster.gdxexamples.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.fasthamster.gdxexamples.ReloadTextureToShader;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.vSyncEnabled = false;        // disable vsync
        config.height = 720;
        config.width = 1024;
//		new LwjglApplication(new gdxExamples(), config);
//        new LwjglApplication(new ShadowMapping(), config);
//        new LwjglApplication(new SphereGlow(), config);
//        new LwjglApplication(new SpherePointLight(), config);
//        new LwjglApplication(new FlatLight(), config);
//        new LwjglApplication(new BoneAnimation(), config);
//        new LwjglApplication(new LowPolySky(), config);
//        new LwjglApplication(new LowPolyBonfire(), config);
//        new LwjglApplication(new Bonfire(), config);
//        new LwjglApplication(new DelaunayTriangulatorTest(), config);
//        new LwjglApplication(new Precipitation(), config);
//        new LwjglApplication(new RainTexture(), config);
        new LwjglApplication(new ReloadTextureToShader(), config);
	}
}
