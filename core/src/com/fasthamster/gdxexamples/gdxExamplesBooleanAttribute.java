package com.fasthamster.gdxexamples;

import com.badlogic.gdx.graphics.g3d.Attribute;

/**
 * Created by alex on 12.05.16.
 */
public class gdxExamplesBooleanAttribute extends Attribute {

    public final static String FlatLightAlias = "flatlight";


    public final static long FlatLight = register(FlatLightAlias);


    public boolean value;


    public gdxExamplesBooleanAttribute(final long type, final boolean value) {
        super(type);
        this.value = value;
    }

    // Constructor for copy() method
    public gdxExamplesBooleanAttribute(gdxExamplesBooleanAttribute other) {
        this(other.type, other.value);
    }

    @Override
    public Attribute copy() { return new gdxExamplesBooleanAttribute(this); }

    @Override
    public boolean equals(Attribute other) {
        return ((gdxExamplesBooleanAttribute)other).value == value;
    }

    @Override
    public int compareTo(Attribute o) {
        if(type != o.type) return (int)(type - o.type);
        boolean otherValue = ((gdxExamplesBooleanAttribute)o).value;
        if(value != otherValue) return value ? 1 : -1;
        return 0;
    }
}
