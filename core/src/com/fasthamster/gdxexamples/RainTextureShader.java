package com.fasthamster.gdxexamples;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g3d.Attributes;
import com.badlogic.gdx.graphics.g3d.Renderable;
import com.badlogic.gdx.graphics.g3d.Shader;
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute;
import com.badlogic.gdx.graphics.g3d.shaders.BaseShader;
import com.badlogic.gdx.graphics.g3d.shaders.DefaultShader;
import com.badlogic.gdx.graphics.g3d.utils.RenderContext;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by alex on 05.11.16.
 */

public class RainTextureShader extends BaseShader {

    private final int u_projViewTrans = register(new Uniform("u_projViewTrans"), DefaultShader.Setters.projViewTrans);
    private final int u_worldTrans = register(new Uniform("u_worldTrans"), DefaultShader.Setters.worldTrans);
    private final int u_diffuseTexture = register(new Uniform("u_diffuseTexture", TextureAttribute.Diffuse), DefaultShader.Setters.diffuseTexture);

    private final int u_velocity = register(new Uniform("u_velocity"));
    private final int u_factor = register(new Uniform("u_factor"));
    private final int u_time = register(new Uniform("u_time"));


    private Vector2 velocity;
    private float factor;

    private Renderable renderable;
    private ShaderProgram program;


    // Constructor
    public RainTextureShader(final Renderable renderable, final ShaderProgram program) {

        Gdx.app.log(Bonfire.TAG, "RainTextureShader()");

        this.program = program;
        this.renderable = renderable;

    }

    // Setters
    public void setUVVelocity(Vector2 v) { this.velocity = v; }
    public void setAlpaFactor(float f) { this.factor = f; }

    @Override
    public void init() {

        final ShaderProgram program = this.program;
        this.program = null;
        init(program, renderable);
        renderable = null;

    }

    @Override
    public void begin(final Camera camera, final RenderContext context) {

        super.begin(camera, context);

        set(u_time, (System.nanoTime() - RainTexture.globalStartTime) / 1000000000f);
        set(u_velocity, velocity);
        set(u_factor, factor);

    }

    @Override
    public void render(Renderable renderable, Attributes combinedAttributes) {

        super.render(renderable, combinedAttributes);

    }

    @Override
    public void end() {

        super.end();

    }

    @Override
    public int compareTo(Shader other) {
        return 0;
    }

    @Override
    public boolean canRender(Renderable renderable) {

        return true;

    }
}
