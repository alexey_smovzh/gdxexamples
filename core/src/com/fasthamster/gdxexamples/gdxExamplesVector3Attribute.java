package com.fasthamster.gdxexamples;

import com.badlogic.gdx.graphics.g3d.Attribute;
import com.badlogic.gdx.math.Vector3;

/**
 * Created by alex on 20.01.16.
 */
public class gdxExamplesVector3Attribute extends Attribute {

    public final static String ParticleVelocityAlias = "ParticleVelocity";

    public final static long ParticleVelocity = register(ParticleVelocityAlias);

    protected static long ParticleVelocityMask = ParticleVelocity;

    public Vector3 value;


    public gdxExamplesVector3Attribute(final long type, final Vector3 value) {
        super(type);
        this.value = value;
    }

    // COnstructor for copy() method
    public gdxExamplesVector3Attribute(gdxExamplesVector3Attribute other) {
        this(other.type, other.value);
    }

    @Override
    public Attribute copy() { return new gdxExamplesVector3Attribute(this); }

    @Override
    public boolean equals(Attribute other) {
        return ((gdxExamplesVector3Attribute)other).value == value;
    }

    @Override
    public int compareTo(Attribute o) {
        if(type != o.type) return type < o.type ? -1 : 1;
        Vector3 otherValue = ((gdxExamplesVector3Attribute)o).value;
        return value.equals(otherValue) ? 0 : (value.len() < otherValue.len() ? -1 : 1);
    }
}
