package com.fasthamster.gdxexamples;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.Renderable;
import com.badlogic.gdx.graphics.g3d.loader.G3dModelLoader;
import com.badlogic.gdx.graphics.g3d.utils.AnimationController;
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController;
import com.badlogic.gdx.utils.UBJsonReader;

/**
 * Created by alex on 30.05.16.
 */
public class BoneAnimation extends ApplicationAdapter {

    public static final String TAG = BoneAnimation.class.getName();

    private ModelInstance airship;
    private ModelBatch batch;
    private PerspectiveCamera camera;

    private AnimationController animation;
    private BoneAnimationShader shader;
    private CameraInputController controller;

    private boolean paused;



    @Override
    public void create () {

        batch = new ModelBatch();

        setupCam();
        setupModels();
        setupShader();

        paused = false;

    }

    private void setupShader() {

        Renderable r = new Renderable();
        airship.getRenderable(r);
        shader = new BoneAnimationShader(r);
        shader.init();

    }

    private void setupCam() {

        camera = new PerspectiveCamera(50f, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.position.set(12f, 0f, 0f);
        camera.lookAt(0f, 0f, 0f);
        camera.near = 0.1f;
        camera.far = 100f;
        camera.update();

        controller = new CameraInputController(camera);
        Gdx.input.setInputProcessor(controller);

    }

    private void setupModels() {

        UBJsonReader jsonReader = new UBJsonReader();
        G3dModelLoader modelLoader = new G3dModelLoader(jsonReader);

        Model model = modelLoader.loadModel(Gdx.files.internal("airship.g3db"));
        airship = new ModelInstance(model);

        animation = new AnimationController(airship);
        animation.setAnimation("Propeller", -1);

    }

    @Override
    public void render () {

        if(!paused) {

            controller.update();

            // Render cycle
            Gdx.gl.glClearColor(0.5f, 0.6f, 0.7f, 1.0f);
            Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

            animation.update(Gdx.graphics.getDeltaTime());

            batch.begin(camera);
            batch.render(airship, shader);
            batch.end();

        }
    }

    @Override
    public void pause() {

        paused = true;

    }

    @Override
    public void resume() {

        paused = false;

    }

    @Override
    public void dispose() {

        batch.dispose();
        shader.dispose();
        airship.nodes.clear();

    }
}