package com.fasthamster.gdxexamples;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.Renderable;
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute;
import com.badlogic.gdx.graphics.g3d.loader.G3dModelLoader;
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.UBJsonReader;

/**
 * Created by alex on 19.07.16.
 */
public class LowPolySky extends ApplicationAdapter {

    public static final String TAG = LowPolySky.class.getName();

    private ModelInstance sky;
    private LowPolySkyShader shader;
    private ModelBatch batch;
    private PerspectiveCamera camera;

    private CameraInputController controller;

    public static long globalStartTime;

    private boolean paused;

    private static final Vector3 POSITION = new Vector3(0f, 0f, 0f);


    @Override
    public void create () {

        globalStartTime = System.nanoTime();

        batch = new ModelBatch();

        setupCam();
        sky = setupSky(5f);
//        setupModels();
        setupShader();

        paused = false;

    }

    private void setupShader() {

        Renderable r = new Renderable();
        sky.getRenderable(r);
        shader = new LowPolySkyShader(r);
        shader.init();

    }

    private void setupCam() {

        camera = new PerspectiveCamera(67f, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.position.set(12f, 0f, 0f);
        camera.lookAt(0f, 0f, 0f);
        camera.near = 0.1f;
        camera.far = 100f;
        camera.update();

        controller = new CameraInputController(camera);
        Gdx.input.setInputProcessor(controller);

    }

    private void setupModels() {

        UBJsonReader jsonReader = new UBJsonReader();
        G3dModelLoader modelLoader = new G3dModelLoader(jsonReader);

        Model model = modelLoader.loadModel(Gdx.files.internal("sky.g3db"));
        sky = new ModelInstance(model);

        // Material
        Texture diffuse = new Texture(Gdx.files.internal("starfield.png"));
        diffuse.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

        sky.materials.get(0).set(TextureAttribute.createDiffuse(diffuse));

    }

    // Creates rectangle with starfield and normal textures
    private ModelInstance setupSky(final float side) {

        // Material
        Texture diffuse = new Texture(Gdx.files.internal("starfield.png"));
        diffuse.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        Texture normal = new Texture(Gdx.files.internal("normal.png"));

        Material material = new Material();
        material.set(TextureAttribute.createDiffuse(diffuse));
        material.set(TextureAttribute.createNormal(normal));

        ModelBuilder builder = new ModelBuilder();
        final Model rect = builder.createRect(
                side, -side, 0f,
                -side, -side, 0f,
                -side, side, 0f,
                side, side, 0f,
                0,
                1,
                0,
                material, VertexAttributes.Usage.Position | VertexAttributes.Usage.TextureCoordinates);

        ModelInstance instance = new ModelInstance(rect);
//        instance.transform.setToTranslation(POSITION);

        return instance;
    }

    @Override
    public void render () {

        if(!paused) {

            controller.update();

            // Render cycle
            Gdx.gl.glClearColor(0f, 0f, 0f, 1.0f);
            Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

            batch.begin(camera);
            batch.render(sky, shader);
            batch.end();

        }
    }

    @Override
    public void pause() {

        paused = true;

    }

    @Override
    public void resume() {

        paused = false;

    }

    @Override
    public void dispose() {

        batch.dispose();
        shader.dispose();
        sky.nodes.clear();

    }
}