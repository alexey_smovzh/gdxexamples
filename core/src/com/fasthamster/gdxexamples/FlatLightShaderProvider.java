package com.fasthamster.gdxexamples;

import com.badlogic.gdx.graphics.g3d.Renderable;
import com.badlogic.gdx.graphics.g3d.Shader;
import com.badlogic.gdx.graphics.g3d.utils.DefaultShaderProvider;

/**
 * Created by alex on 12.05.16.
 */
public class FlatLightShaderProvider extends DefaultShaderProvider {

    public void clear() {
        for(final Shader shader : shaders)
            shader.dispose();
        shaders.clear();
    }

    @Override
    protected Shader createShader(Renderable renderable) {

        if(renderable.material.has(gdxExamplesBooleanAttribute.FlatLight))
            return new FlatLightShader(renderable);

        return super.createShader(renderable);

    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
