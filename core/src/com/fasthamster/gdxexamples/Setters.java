package com.fasthamster.gdxexamples;

import com.badlogic.gdx.graphics.g3d.Attributes;
import com.badlogic.gdx.graphics.g3d.Renderable;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute;
import com.badlogic.gdx.graphics.g3d.shaders.BaseShader;
import com.badlogic.gdx.math.Matrix3;

/**
 * Created by alex on 20.01.16.
 */
public final class Setters {
    public final static BaseShader.Setter projViewTrans = new BaseShader.GlobalSetter() {
        @Override
        public void set (BaseShader shader, int inputID, Renderable renderable, Attributes combinedAttributes) {
            shader.set(inputID, shader.camera.combined);
        }
    };
    public final static BaseShader.Setter worldTrans = new BaseShader.LocalSetter() {
        @Override
        public void set (BaseShader shader, int inputID, Renderable renderable, Attributes combinedAttributes) {
            shader.set(inputID, renderable.worldTransform);
        }
    };
    public final static BaseShader.Setter normalMatrix = new BaseShader.LocalSetter() {
        private final Matrix3 tmpM = new Matrix3();

        @Override
        public void set (BaseShader shader, int inputID, Renderable renderable, Attributes combinedAttributes) {
            shader.set(inputID, tmpM.set(renderable.worldTransform).inv().transpose());
        }
    };
    public final static BaseShader.Setter diffuseTexture = new BaseShader.LocalSetter() {
        @Override
        public void set (BaseShader shader, int inputID, Renderable renderable, Attributes combinedAttributes) {
            final int unit = shader.context.textureBinder.bind(((TextureAttribute)(combinedAttributes.get(TextureAttribute.Diffuse))).textureDescription);
            shader.set(inputID, unit);
        }
    };
    public final static BaseShader.Setter diffuseColor = new BaseShader.LocalSetter() {
        @Override
        public void set (BaseShader shader, int inputID, Renderable renderable, Attributes combinedAttributes) {
            shader.set(inputID, ((ColorAttribute)(combinedAttributes.get(ColorAttribute.Diffuse))).color);
        }
    };
    public final static BaseShader.Setter uvTexture = new BaseShader.LocalSetter() {
        @Override
        public void set(BaseShader shader, int inputID, Renderable renderable, Attributes combinedAttributes) {
            shader.set(inputID, ((gdxExamplesVector2Attribute)(combinedAttributes.get(gdxExamplesVector2Attribute.UVTexture))).value);
        }
    };
    public final static BaseShader.Setter uvCoord = new BaseShader.LocalSetter() {
        @Override
        public void set(BaseShader shader, int inputID, Renderable renderable, Attributes combinedAttributes) {
            shader.set(inputID, ((gdxExamplesVector2Attribute)(combinedAttributes.get(gdxExamplesVector2Attribute.UVCoord))).value);
        }
    };
    public final static BaseShader.Setter particleVelocity = new BaseShader.LocalSetter() {
        @Override
        public void set(BaseShader shader, int inputID, Renderable renderable, Attributes combinedAttributes) {
            shader.set(inputID, ((gdxExamplesVector3Attribute)(combinedAttributes.get(gdxExamplesVector3Attribute.ParticleVelocity))).value);
        }
    };
    public final static BaseShader.Setter particleStartTime = new BaseShader.LocalSetter() {
        @Override
        public void set(BaseShader shader, int inputID, Renderable renderable, Attributes combinedAttributes) {
            shader.set(inputID, ((gdxExamplesFloatAttribute)(combinedAttributes.get(gdxExamplesFloatAttribute.ParticleStartTime))).value);
        }
    };
    public final static BaseShader.Setter particleStartColor = new BaseShader.LocalSetter() {
        @Override
        public void set(BaseShader shader, int inputID, Renderable renderable, Attributes combinedAttributes) {
            shader.set(inputID, ((gdxExamplesColorAttribute)(combinedAttributes.get(gdxExamplesColorAttribute.ParticleStartColor))).value);
        }
    };
    public final static BaseShader.Setter particleEndColor = new BaseShader.LocalSetter() {
        @Override
        public void set(BaseShader shader, int inputID, Renderable renderable, Attributes combinedAttributes) {
            shader.set(inputID, ((gdxExamplesColorAttribute)(combinedAttributes.get(gdxExamplesColorAttribute.ParticleEndColor))).value);
        }
    };
}




