package com.fasthamster.gdxexamples;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g3d.Attributes;
import com.badlogic.gdx.graphics.g3d.Renderable;
import com.badlogic.gdx.graphics.g3d.Shader;
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute;
import com.badlogic.gdx.graphics.g3d.shaders.BaseShader;
import com.badlogic.gdx.graphics.g3d.shaders.DefaultShader;
import com.badlogic.gdx.graphics.g3d.utils.RenderContext;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

/**
 * Created by alex on 03.11.16.
 */

public class PrecipitationShader extends BaseShader {

    private final int u_projViewTrans = register(new Uniform("u_projViewTrans"), DefaultShader.Setters.projViewTrans);
    private final int u_worldTrans = register(new Uniform("u_worldTrans"), DefaultShader.Setters.worldTrans);
    private final int u_diffuseTexture = register(new Uniform("u_diffuseTexture", TextureAttribute.Diffuse), DefaultShader.Setters.diffuseTexture);

    private final int u_uvCoord = register(new Uniform("u_uvCoord"));
    private final int u_particleStartTime = register(new Uniform("u_particleStartTime"));
    private final int u_particleVelocity = register(new Uniform("u_particleVelocity"));
    private final int u_time = register(new Uniform("u_time"));


    private Vector2 uv;
    private float startTime;
    private Vector3 velocity;

    private Renderable renderable;
    private ShaderProgram program;


    // Constructor
    public PrecipitationShader(final Renderable renderable, final ShaderProgram program) {

        Gdx.app.log(Bonfire.TAG, "PrecipitationShader()");

        this.program = program;
        this.renderable = renderable;

    }

    // Setters
    public void setUV(Vector2 uv) { this.uv = uv; }
    public void setStartTime(float t) { this.startTime = t; }
    public void setVelocity(Vector3 v) { this.velocity = v; }

    @Override
    public void init() {

        final ShaderProgram program = this.program;
        this.program = null;
        init(program, renderable);
        renderable = null;

    }

    @Override
    public void begin(final Camera camera, final RenderContext context) {

        super.begin(camera, context);

        set(u_time, (System.nanoTime() - Precipitation.globalStartTime) / 1000000000f);
        set(u_uvCoord, uv);
        set(u_particleStartTime, startTime);
        set(u_particleVelocity, velocity);
    }

    @Override
    public void render(Renderable renderable, Attributes combinedAttributes) {

        super.render(renderable, combinedAttributes);

    }

    @Override
    public void end() {

        super.end();

    }

    @Override
    public int compareTo(Shader other) {
        return 0;
    }

    @Override
    public boolean canRender(Renderable renderable) {

        return true;

    }

}
