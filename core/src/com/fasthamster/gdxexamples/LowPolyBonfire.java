package com.fasthamster.gdxexamples;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.Renderable;
import com.badlogic.gdx.graphics.g3d.loader.G3dModelLoader;
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.UBJsonReader;

/**
 * Created by alex on 23.07.16.
 */
public class LowPolyBonfire extends ApplicationAdapter {

    public static final String TAG = LowPolyBonfire.class.getName();

    private ModelInstance bonfire;
    private LowPolyBonfireShader shader;
    private ModelBatch batch;
    private PerspectiveCamera camera;

    private CameraInputController controller;

    public static long globalStartTime;

    private boolean paused;

    private static final Vector3 POSITION = new Vector3(0f, 0f, 0f);


    @Override
    public void create () {

        globalStartTime = System.nanoTime();

        batch = new ModelBatch();

        setupCam();
        setupModels();
        setupShader();

        paused = false;

    }

    private void setupShader() {

        Renderable r = new Renderable();
        bonfire.getRenderable(r);
        shader = new LowPolyBonfireShader(r);
        shader.init();

    }

    private void setupCam() {

        camera = new PerspectiveCamera(67f, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.position.set(12f, 0f, 0f);
        camera.lookAt(0f, 0f, 0f);
        camera.near = 0.1f;
        camera.far = 100f;
        camera.update();

        controller = new CameraInputController(camera);
        Gdx.input.setInputProcessor(controller);

    }

    private void setupModels() {

        UBJsonReader jsonReader = new UBJsonReader();
        G3dModelLoader modelLoader = new G3dModelLoader(jsonReader);

        Model model = modelLoader.loadModel(Gdx.files.internal("bonfire.g3db"));
        bonfire = new ModelInstance(model);

    }

    @Override
    public void render () {

        if(!paused) {

            controller.update();

            // Render cycle
            Gdx.gl.glClearColor(0f, 0f, 0f, 1.0f);
            Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

            batch.begin(camera);
            batch.render(bonfire, shader);
            batch.end();

        }
    }

    @Override
    public void pause() {

        paused = true;

    }

    @Override
    public void resume() {

        paused = false;

    }

    @Override
    public void dispose() {

        batch.dispose();
        shader.dispose();
        bonfire.nodes.clear();

    }
}