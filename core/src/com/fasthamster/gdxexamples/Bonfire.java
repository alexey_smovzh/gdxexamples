package com.fasthamster.gdxexamples;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.Renderable;
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute;
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;

import java.util.Random;

/**
 * Created by alex on 21.01.16.
 */
public class Bonfire extends ApplicationAdapter {

    public static final String TAG = Bonfire.class.getName();

    private ModelBatch batch;
    private PerspectiveCamera camera;
    private CameraInputController controller;
    private ShaderProgram program;
    private Mesh mesh;

    private Array<Particle> particles = new Array<Particle>();

    private Texture diffuse;
    private Material material;

    public static long globalStartTime;
    private Random rand = new Random();
    private boolean paused;


    private static final long FPS = 30l;
    private static final float SCALE = 0.5f;
    private static final float DEPTH = 0.5f;
    private static final String TEXTURE = "flame.png";
    private static final Vector2 UV_TEXTURE = new Vector2(4f, 4f);          // Count of texture rows and cols
    private static final Vector3 START_POINT = new Vector3(0f, 0f, -0.5f);
    private static final Vector3 VELOCITY = new Vector3(0f, 0.6f, 0f);      // Move vector and speed
    private static final float LIFE_TIME = 4f;
    private static final int MAX_PARTICLES = 20;
    private static final float PARTICLES_INTERVAL = LIFE_TIME * VELOCITY.y / MAX_PARTICLES;
    private static final float STEP = VELOCITY.y / FPS;


    public class Particle {
        public Renderable renderable;
        public BonfireShader shader;
        public float startTime;
        public float lifeTime;

        public Particle(Renderable renderable, BonfireShader shader,
                        float startTime, float lifeTime) {
            this.renderable = renderable;
            this.shader = shader;
            this.startTime = startTime;
            this.lifeTime = lifeTime;
        }
    }

    @Override
    public void create () {

        globalStartTime = System.nanoTime();

        setupCam();
        createMesh();
        setupShader();
        loadTexture();
        batch = new ModelBatch();

        paused = false;

    }

    private void createMesh() {

        mesh = new Mesh(true,
                4, 6,
                new VertexAttribute(VertexAttributes.Usage.Position, 3, "a_position"),
                new VertexAttribute(VertexAttributes.Usage.TextureCoordinates, 2, "a_texCoord0"));
        // Vertices + normals
        mesh.setVertices(new float[]{
                1f, -1f, 0f, 0f, 1f,
                -1f, -1f, 0f, 1f, 1f,
                -1f, 1f, 0f, 1f, 0f,
                1f, 1f, 0f, 0f, 0f});

        mesh.setIndices(new short[]{0, 1, 2, 2, 3, 0});

    }

    private void loadTexture() {

        diffuse = new Texture(Gdx.files.internal(TEXTURE));
        diffuse.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        material = new Material(new TextureAttribute(TextureAttribute.createDiffuse(diffuse)));

    }

    private void setupShader() {

        String vert = Gdx.files.internal("bonfire.vert").readString();
        String frag = Gdx.files.internal("bonfire.frag").readString();

        program = new ShaderProgram(vert, frag);

        if(program.getLog() != null) Gdx.app.log(TAG, program.getLog());

    }

    private void setupCam() {

        camera = new PerspectiveCamera(67f, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.position.set(0f, 0f, 4f);
        camera.lookAt(0f, 0f, 0f);
        camera.near = 0.1f;
        camera.far = 100f;
        camera.update();

        controller = new CameraInputController(camera);
        Gdx.input.setInputProcessor(controller);

    }

    private float getCurrentTime() {
        return (System.nanoTime() - globalStartTime) / 1000000000f;
    }

    int i = 1;
    private void emit() {
        // Take next flame texture coord
        Vector2 uvCoord = calcUVMapping(i);
        i++;
        if(i > 16) i = 1;

        // Start time
        float startTime = getCurrentTime();

        // Little depth variance, SIDE limit for create bonfire place look like a box
        float depth = rand.nextFloat() * DEPTH;

        // Create particle
        Renderable particle = createRenderable();

        BonfireShader shader = new BonfireShader(particle, program);
        shader.setUV(uvCoord);
        shader.setStartTime(startTime);
        shader.setVelocity(VELOCITY);
        shader.init();

        // Assign shader
        particle.shader = shader;

        particle.worldTransform.translate(new Vector3(0f, 0f, depth));

        // Create particle
        particles.add(new Particle(particle, shader, startTime, LIFE_TIME));

    }

    private Renderable createRenderable() {

        Renderable renderable = new Renderable();
        renderable.meshPart.primitiveType = GL20.GL_TRIANGLES;
        renderable.meshPart.offset = 0;
        renderable.meshPart.size = mesh.getNumIndices();
        renderable.material = material;
        renderable.meshPart.mesh = mesh;

        return renderable;

    }

    // Calculates texture coordinates offsets for vertex shader
    private Vector2 calcUVMapping(int num) {

        int row, column;

        column = (num / (int)UV_TEXTURE.x);                         // column num, starts from 0

        if(num > (int)UV_TEXTURE.x) {
            row = num - (column * (int)UV_TEXTURE.x);
        } else {
            row = num;
        }

        return new Vector2(1f / UV_TEXTURE.x * column, 1f / UV_TEXTURE.y * row);
    }

    // Render model
    float delta = 0f;
    float time;
    @Override
    public void render() {

        if(!paused) {

            // Update
            if(particles.size < MAX_PARTICLES) {
                if (delta > PARTICLES_INTERVAL) {
                    emit();
                    delta = 0f;
                }

                delta += STEP;
            }

            Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

            batch.begin(camera);

            Gdx.gl.glEnable(GL20.GL_BLEND);
            Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);


            for(int i = 0; i < particles.size; i++) {
                Particle p = particles.get(i);

                if(getCurrentTime() - p.startTime > p.lifeTime) {
                    time = getCurrentTime();
                    p.startTime = time;
                    p.shader.setStartTime(time);
                }

                batch.render(p.renderable);
            }

            batch.end();

            sleep(FPS);
        }
    }

    // Sleep for power consumption
    private long diff, start = System.currentTimeMillis();
    private void sleep(long fps) {

        diff = System.currentTimeMillis() - start;
        long targetDelay = 1000l / fps;
        if(diff < targetDelay) {
            try {
                Thread.sleep(targetDelay - diff);
            } catch (InterruptedException e) {	}
        }
        start = System.currentTimeMillis();
    }

    public void dispose() {

        particles.clear();
        if(material != null) material.clear();
        if(diffuse != null) diffuse.dispose();

    }
}
