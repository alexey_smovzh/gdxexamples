package com.fasthamster.gdxexamples;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.Renderable;
import com.badlogic.gdx.graphics.g3d.attributes.BlendingAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Vector2;

import java.util.Random;

/**
 * Created by alex on 05.11.16.
 */

public class RainTexture extends ApplicationAdapter {


    public static final String TAG = RainTexture.class.getName();

    private ModelBatch batch;
    private PerspectiveCamera camera;
    private ShaderProgram program;
    private RainTextureShader shader;
    private Mesh mesh;
    private Renderable rain;

    private SpriteBatch spriteBatch;
    private Texture backgroud;

    private Material material;
    private Texture texture;


    private Vector2 uvVelocity = new Vector2();
    public static long globalStartTime;
    private Random rand = new Random();
    private boolean running;

    private static final String TEXTURE = "rain.png";
//    private static final String TEXTURE = "snow.png";
    private static final float V_VELOCITY = -0.2f;
    private static final float U_MIN_VELOCITY = -0.2f;
    private static final float U_MAX_VELOCITY = 0.2f;


    @Override
    public void create () {

        globalStartTime = System.nanoTime();

        setupCam();
        setupShaderProgram();
        setupMesh(2f, 2f);
        setupMaterial();
        setupBackgroud();

        setupRain();


        batch = new ModelBatch();

        start();

    }

    private void setupCam() {

        camera = new PerspectiveCamera(67f, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.position.set(0f, 0f, 2f);
        camera.lookAt(0f, 0f, 0f);
        camera.near = 0.1f;
        camera.far = 100f;
        camera.update();

    }

    private void setupBackgroud() {

        spriteBatch = new SpriteBatch();

        backgroud = new Texture(Gdx.files.internal("winter_mountain.png"));
        backgroud.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);


    }

    // Setup shaderprogram at once
    private void setupShaderProgram() {

        String vert = Gdx.files.internal("raintexture.vert").readString();
        String frag = Gdx.files.internal("raintexture.frag").readString();

        program = new ShaderProgram(vert, frag);

    }

    // Generate particle mesh
    private void setupMesh(float w, float h) {

        mesh = new Mesh(true,
                4, 6,
                new VertexAttribute(VertexAttributes.Usage.Position, 3, "a_position"),
                new VertexAttribute(VertexAttributes.Usage.TextureCoordinates, 2, "a_texCoord0"));

        mesh.setVertices(new float[]{           // Vertices + UVs
                w, -h, 0f, 0f, 1f,
                -w, -h, 0f, 1f, 1f,
                -w, h, 0f, 1f, 0f,
                w, h, 0f, 0f, 0f});

        mesh.setIndices(new short[]{0, 1, 2, 2, 3, 0});

    }

    // Load texture at once
    private void setupMaterial() {

        texture = new Texture(Gdx.files.internal(TEXTURE));
        texture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        texture.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat);

        material = new Material(new BlendingAttribute(GL20.GL_ONE, GL20.GL_ONE_MINUS_SRC_ALPHA, 1f),
                TextureAttribute.createDiffuse(texture));

    }


    // Emit particles
    private void setupRain() {

        rain = createRenderable();

        shader = new RainTextureShader(rain, program);
        shader.setUVVelocity(uvVelocity);
        shader.setAlpaFactor(1f);
        shader.init();

        rain.shader = shader;                                               // Assign shader

    }

    private Renderable createRenderable() {

        Renderable renderable = new Renderable();
        renderable.meshPart.primitiveType = GL20.GL_TRIANGLES;
        renderable.meshPart.offset = 0;
        renderable.meshPart.size = mesh.getNumIndices();
        renderable.material = material;
        renderable.meshPart.mesh = mesh;

        // Random X position
        renderable.meshPart.center.set(0f, 4f, 0f);      // for RenderableSorter

        return renderable;
    }


    // Controls
    public void start() {

        // random rain angle
        uvVelocity.x = rand.nextFloat() * (U_MAX_VELOCITY - U_MIN_VELOCITY) + U_MIN_VELOCITY;
        uvVelocity.y = V_VELOCITY;

        running = true;
    }

    public void stop() {

        running = false;

    }

    float alpha = 1f;
    // Render model
    public void render() {

        if(running == true) {

            if(alpha > 0.01f) {
                shader.setAlpaFactor(alpha);
                alpha -= 0.001f;

                if(alpha < 0.01f) alpha = 0.01f;
            }


            Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

            spriteBatch.begin();
            spriteBatch.draw(backgroud, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
            spriteBatch.end();

            batch.begin(camera);

            Gdx.gl.glEnable(GL20.GL_BLEND);
            Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);

            batch.render(rain);
            batch.end();

        }
    }

    public void dispose() {

        if(material != null) material.clear();
        if(texture != null) texture.dispose();
        if(program != null) program.dispose();

    }
}
