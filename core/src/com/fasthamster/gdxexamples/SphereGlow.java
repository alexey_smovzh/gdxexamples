package com.fasthamster.gdxexamples;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g3d.Attribute;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.Renderable;
import com.badlogic.gdx.graphics.g3d.Shader;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController;
import com.badlogic.gdx.graphics.g3d.utils.DefaultShaderProvider;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;

/**
 * Created by alex on 07.02.16.
 */
public class SphereGlow extends ApplicationAdapter {

    public static final String TAG = SphereGlow.class.getName();

    private ModelInstance sphere;
    private PerspectiveCamera camera;
    private ModelBatch modelBatch;
    private ModelBuilder builder;
    private ShaderProgram shader;
    private CameraInputController camController;


    @Override
    public void create () {

        builder = new ModelBuilder();

        setupCam();
        setupSphere();
//        setupShader();
        modelBatch = new ModelBatch();



        camController = new CameraInputController(camera);
        Gdx.input.setInputProcessor(camController);

    }

    private void setupShader() {

        String vert = Gdx.files.internal("GlowShader.vert").readString();
        String frag = Gdx.files.internal("GlowShader.frag").readString();

        shader = new ShaderProgram(vert, frag);

        if(!shader.isCompiled())
            Gdx.app.log(TAG, "Shader error: " + shader.getLog());

        modelBatch = new ModelBatch(new DefaultShaderProvider() {
            @Override
            protected Shader createShader(final Renderable renderable) {
                return new DepthShader(renderable, shader);
            }
        });

    }

    private void setupSphere() {

        Material material = new Material();
        Attribute diffuse = new ColorAttribute(ColorAttribute.Diffuse, Color.ORANGE);
        Attribute emissive = new ColorAttribute(ColorAttribute.Emissive, new Color(0f, 0f, 2f, 1f));
        material.set(emissive, diffuse);

        Model model = builder.createSphere(2f, 2f, 2f, 16, 16, material, VertexAttributes.Usage.Position | VertexAttributes.Usage.TextureCoordinates | VertexAttributes.Usage.Normal);

        sphere = new ModelInstance(model);

    }

    private void setupCam() {

        camera = new PerspectiveCamera(67f, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.position.set(0f, 0f, 8f);
        camera.lookAt(0f, 0f, 0f);
        camera.near = 0.1f;
        camera.far = 100f;
        camera.update();

    }

    @Override
    public void render () {

        camController.update();

        Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

        Gdx.gl.glEnable(GL20.GL_BLEND);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);

//        int lp = shader.getUniformLocation("u_lightPos");
//        shader.begin();
//        shader.setUniformf(lp, camera.position);
//        shader.end();


        modelBatch.begin(camera);
        modelBatch.render(sphere);
        modelBatch.end();

    }

    @Override
    public void dispose() {

        if(shader != null)
            shader.dispose();

        modelBatch.dispose();

    }
}
