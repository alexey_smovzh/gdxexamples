package com.fasthamster.gdxexamples;

import com.badlogic.gdx.graphics.g3d.Attribute;

/**
 * Created by alex on 20.01.16.
 */
public class gdxExamplesFloatAttribute extends Attribute {

    public final static String ParticleStartTimeAlias = "particleStartTimeAlias";

    public final static long ParticleStartTime = register(ParticleStartTimeAlias);

    protected static long ParticleStartTimeMask = ParticleStartTime;


    public float value;


    public gdxExamplesFloatAttribute(final long type, final float value) {
        super(type);
        this.value = value;
    }

    // Constructor for copy() method
    public gdxExamplesFloatAttribute(gdxExamplesFloatAttribute other) {
        this(other.type, other.value);
    }

    @Override
    public Attribute copy() { return new gdxExamplesFloatAttribute(this); }

    @Override
    public boolean equals(Attribute other) {
        return ((gdxExamplesFloatAttribute)other).value == value;
    }

    @Override
    public int compareTo(Attribute o) {
        if(type != o.type) return type < o.type ? -1 : 1;
        float otherValue = ((gdxExamplesFloatAttribute)o).value;
        return value == otherValue ? 0 : (value < otherValue ? -1 : 1);
    }
}


