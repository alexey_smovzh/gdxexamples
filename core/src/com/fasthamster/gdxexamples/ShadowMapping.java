package com.fasthamster.gdxexamples;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.Renderable;
import com.badlogic.gdx.graphics.g3d.Shader;
import com.badlogic.gdx.graphics.g3d.loader.G3dModelLoader;
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController;
import com.badlogic.gdx.graphics.g3d.utils.DefaultShaderProvider;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.UBJsonReader;

/**
 * Created by alex on 04.02.16.
 */
public class ShadowMapping extends ApplicationAdapter {

    public static final String TAG = ShadowMapping.class.getName();

    private ModelBatch colorModelBatch;
    private ModelBatch depthModelBatch;

    private ShaderProgram depthShader;
    private ShaderProgram colorShader;

    private PerspectiveCamera camera;
    private PerspectiveCamera depth;
    private CameraInputController camController;
    private ModelInstance scene;
    private FrameBuffer frameBuffer;


    private Texture depthMap;

    private static final int DEPTHMAPSIZE = 1024;



    @Override
    public void create () {

        setupCam();
        setupDepthCam();
        setupColorShader();
        setupDepthShader();
        loadModel();

    }

    private void setupCam() {

        camera = new PerspectiveCamera(67f, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.position.set(0f, 4f, 12f);
        camera.lookAt(0f, 0f, 0f);
        camera.near = 0.1f;
        camera.far = 100f;
        camera.update();

        camController = new CameraInputController(camera);
        Gdx.input.setInputProcessor(camController);

    }

    private void setupDepthCam() {

        depth = new PerspectiveCamera(120f, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        depth.near = 0.1f;
        depth.far = 100f;
        depth.position.set(0f, 10f, 0f);
        depth.lookAt(0f, 0f, 0f);
        depth.update();

    }

    private void setupDepthShader() {

        String depthVert = Gdx.files.internal("DepthShader.vert").readString();
        String depthFrag = Gdx.files.internal("DepthShader.frag").readString();

        depthShader = new ShaderProgram(depthVert, depthFrag);

        if(!depthShader.isCompiled())
            Gdx.app.log(TAG, "Shader error: " + depthShader.getLog());

        depthModelBatch = new ModelBatch(new DefaultShaderProvider() {
            @Override
            protected Shader createShader(final Renderable renderable) {
                return new DepthShader(renderable, depthShader);
            }
        });
    }

    private void setupColorShader() {

        String colorVert = Gdx.files.internal("ColorShader.vert").readString();
        String colorFrag = Gdx.files.internal("ColorShader.frag").readString();

        colorShader = new ShaderProgram(colorVert, colorFrag);

        if(!colorShader.isCompiled())
            Gdx.app.log(TAG, "Shader error: " + colorShader.getLog());

        colorModelBatch = new ModelBatch(new DefaultShaderProvider() {
            @Override
            protected Shader createShader(final Renderable renderable) {
                return new ColorShader(renderable, colorShader);
            }
        });
    }

    private void loadModel() {

        UBJsonReader jsonReader = new UBJsonReader();
        G3dModelLoader modelLoader = new G3dModelLoader(jsonReader);

        Model model = modelLoader.loadModel(Gdx.files.internal("low_poly_scene.g3db"));

        scene = new ModelInstance(model);

        scene.transform.setToRotation(0f, 1f, 0f, -90f);

    }

    private void updateDepth() {
        depth.rotateAround(Vector3.Zero, Vector3.Z, 0.1f);
    }

    private void renderDepth() {

        if(frameBuffer == null)
            frameBuffer = new FrameBuffer(Pixmap.Format.RGBA8888, DEPTHMAPSIZE, DEPTHMAPSIZE, true);

        frameBuffer.begin();
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

        depthShader.begin();
        depthShader.setUniformf("u_cameraFar", depth.far);
        depthShader.setUniformf("u_lightPosition", depth.position);
        depthShader.end();

        depthModelBatch.begin(depth);
        depthModelBatch.render(scene);
        depthModelBatch.end();

        frameBuffer.end();

        depthMap = frameBuffer.getColorBufferTexture();
    }

    private void renderScene() {

        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

        colorShader.begin();
        depthMap.bind(0);
        colorShader.setUniformi("u_depthMap", 0);
        colorShader.setUniformMatrix("u_lightTrans", depth.combined);
        colorShader.setUniformf("u_lightPosition", depth.position);
        colorShader.setUniformf("u_cameraFar", camera.far);
        colorShader.end();

        colorModelBatch.begin(camera);
        colorModelBatch.render(scene);
        colorModelBatch.end();

    }

    @Override
    public void render() {


        camController.update();

        updateDepth();
        renderDepth();
        renderScene();


//        Gdx.gl.glEnable(GL20.GL_BLEND);
//        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);

    }

    @Override
    public void resize(final int width, final int height) {
        camera.viewportWidth = width;
        camera.viewportHeight = height;
        camera.update();
    }

    @Override
    public void dispose() {

        depthMap.dispose();
        depthShader.dispose();
        colorShader.dispose();
        depthModelBatch.dispose();
        colorModelBatch.dispose();

    }
}

