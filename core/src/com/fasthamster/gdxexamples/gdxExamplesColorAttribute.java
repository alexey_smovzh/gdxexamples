package com.fasthamster.gdxexamples;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g3d.Attribute;

/**
 * Created by alex on 21.01.16.
 */
public class gdxExamplesColorAttribute extends Attribute {

    public final static String ParticleStartColorAlias = "ParticleStartColor";
    public final static String ParticleEndColorAlias = "ParticleEndColor";

    public final static long ParticleStartColor = register(ParticleStartColorAlias);
    public final static long ParticleEndColor = register(ParticleEndColorAlias);

    protected static long ParticleColorMask = ParticleStartColor | ParticleEndColor;


    public Color value;


    public gdxExamplesColorAttribute(final long type, final Color color) {
        super(type);
        this.value = color;
    }

    // Constructor for copy() method
    public gdxExamplesColorAttribute(gdxExamplesColorAttribute other) {
        this(other.type, other.value);
    }

    @Override
    public Attribute copy() { return new gdxExamplesColorAttribute(this); }

    @Override
    public boolean equals(Attribute other) {
        return ((gdxExamplesColorAttribute)other).value == value;
    }

    @Override
    public int compareTo(Attribute o) {
        if(type != o.type) return type < o.type ? -1 : 1;
        Color otherValue = ((gdxExamplesColorAttribute)o).value;
        return value.equals(otherValue) ? 0 : (value.a < otherValue.a ? -1 : 1);
    }
}


