package com.fasthamster.gdxexamples;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g3d.Attributes;
import com.badlogic.gdx.graphics.g3d.Renderable;
import com.badlogic.gdx.graphics.g3d.Shader;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.shaders.BaseShader;
import com.badlogic.gdx.graphics.g3d.utils.RenderContext;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;

/**
 * Created by alex on 12.05.16.
 */
public class FlatLightShader extends BaseShader {


    private final int u_projViewTrans = register(new Uniform("u_projViewTrans"), Setters.projViewTrans);
    private final int u_worldTrans = register(new Uniform("u_worldTrans"), Setters.worldTrans);
    private final int u_normalMatrix = register(new Uniform("u_normalMatrix"), Setters.normalMatrix);

    private final int u_diffuseColor = register(new Uniform("u_diffuseColor", ColorAttribute.Diffuse), Setters.diffuseColor);

    private Renderable renderable;


    // Constructor
    public FlatLightShader(final Renderable renderable) {

        Gdx.app.log(FlatLight.TAG, "FlatLightShader()");

        String vert = Gdx.files.internal("flatlight.vert").readString();
        String frag = Gdx.files.internal("flatlight.frag").readString();

        this.program = new ShaderProgram(vert, frag);
        this.renderable = renderable;

    }

    @Override
    public void init() {

        final ShaderProgram program = this.program;
        this.program = null;
        init(program, renderable);
        renderable = null;

    }

    @Override
    public void begin(final Camera camera, final RenderContext context) {
        super.begin(camera, context);

        context.setCullFace(GL20.GL_BACK);
        context.setDepthTest(GL20.GL_LEQUAL);
    }

    @Override
    public void render(Renderable renderable, Attributes combinedAttributes) {

        super.render(renderable, combinedAttributes);

    }

    @Override
    public void end() {

        super.end();

    }

    @Override
    public int compareTo(Shader other) {
        return 0;
    }

    @Override
    public boolean canRender(Renderable renderable) {

        return renderable.material.has(gdxExamplesBooleanAttribute.FlatLight);

    }
}