package com.fasthamster.gdxexamples;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.Renderable;
import com.badlogic.gdx.graphics.g3d.attributes.BlendingAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;

import java.util.Random;

/**
 * Created by alex on 03.11.16.
 */

public class Precipitation extends ApplicationAdapter {

    public static final String TAG = Precipitation.class.getName();

    private ModelBatch batch;
    private PerspectiveCamera camera;
    private ShaderProgram program;
    private Mesh mesh;

    private SpriteBatch spriteBatch;
    private Texture backgroud;

    private Array<Particle> particles = new Array<Particle>();

    private Texture texture;
    private Material material;

    public static long globalStartTime;
    private Random rand = new Random();
    private boolean running;

    private static final float RAIN_WIDTH = 0.02f;
    private static final float RAIN_HEIGHT = RAIN_WIDTH * 4;
    private static final String TEXTURE = "precipitation.png";
    private static final Vector2 UV_TEXTURE = new Vector2(2f, 1f);          // Count of texture cols and rows
    private static final Vector3 START_POINT = new Vector3(0f, 2f, 0f);
    private static final Vector3 VELOCITY = new Vector3(0f, -8f, 0f);      // Move vector and speed
    private static final float LIFE_TIME = 0.4f;
    private static final int MAX_PARTICLES = 600;
    private static final float SCENE_LEFT = -2f;
    private static final float SCENE_RIGHT = 2f;
    private static final float SCALE = 2f;


    // Pre-run calc
    private static final float PARTICLES_INTERVAL = LIFE_TIME * VELOCITY.y / MAX_PARTICLES;
    private static final float STEP = VELOCITY.y / 60f;


    public class Particle {
        public Renderable renderable;
        public PrecipitationShader shader;
        public float startTime;
        public float lifeTime;

        public Particle(Renderable renderable, PrecipitationShader shader,
                        float startTime, float lifeTime) {
            this.renderable = renderable;
            this.shader = shader;
            this.startTime = startTime;
            this.lifeTime = lifeTime;
        }
    }


    @Override
    public void create () {

        globalStartTime = System.nanoTime();

        setupCam();
        setupShaderProgram();
        setupMaterial();
        setupBackgroud();

        batch = new ModelBatch();

        start(true);

    }

    private void setupCam() {

        camera = new PerspectiveCamera(67f, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.position.set(0f, 0f, 2f);
        camera.lookAt(0f, 0f, 0f);
        camera.near = 0.1f;
        camera.far = 100f;
        camera.update();

    }

    private void setupBackgroud() {

        spriteBatch = new SpriteBatch();

        backgroud = new Texture(Gdx.files.internal("winter_mountain.png"));
        backgroud.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);


    }

    private float getCurrentTime() {
        return (System.nanoTime() - globalStartTime) / 1000000000f;
    }

    // Setup shaderprogram at once
    private void setupShaderProgram() {

        String vert = Gdx.files.internal("precipitation.vert").readString();
        String frag = Gdx.files.internal("precipitation.frag").readString();

        program = new ShaderProgram(vert, frag);

    }

    // Generate particle mesh
    private void setupMesh(float w, float h) {

        mesh = new Mesh(true,
                4, 6,
                new VertexAttribute(VertexAttributes.Usage.Position, 3, "a_position"),
                new VertexAttribute(VertexAttributes.Usage.TextureCoordinates, 2, "a_texCoord0"));

        mesh.setVertices(new float[]{           // Vertices + UVs
                w, -h, 0f, 0f, 1f,
                -w, -h, 0f, 1f, 1f,
                -w, h, 0f, 1f, 0f,
                w, h, 0f, 0f, 0f});

        mesh.setIndices(new short[]{0, 1, 2, 2, 3, 0});

    }

    // Load texture at once
    private void setupMaterial() {

        texture = new Texture(Gdx.files.internal(TEXTURE));
        texture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        material = new Material(new BlendingAttribute(GL20.GL_ONE, GL20.GL_ONE_MINUS_SRC_ALPHA, 1f),
                TextureAttribute.createDiffuse(texture));

    }

    // Emit particles
    private void emit() {

        // Start time
        float startTime = getCurrentTime();

        // Create particle
        Renderable particle = createRenderable();

        PrecipitationShader shader = new PrecipitationShader(particle, program);
        shader.setUV(calcUVMapping(0));                                         // if rain = 1, if snow = 2
        shader.setStartTime(startTime);
        shader.setVelocity(VELOCITY);
        shader.init();

        particle.shader = shader;                                               // Assign shader

        particles.add(new Particle(particle, shader, startTime, LIFE_TIME));

    }

    private Renderable createRenderable() {

        Renderable renderable = new Renderable();
        renderable.meshPart.primitiveType = GL20.GL_TRIANGLES;
        renderable.meshPart.offset = 0;
        renderable.meshPart.size = mesh.getNumIndices();
        renderable.material = material;
        renderable.meshPart.mesh = mesh;

        // Random X position
        renderable.worldTransform.translate(getRenderableXPosition(), START_POINT.y, START_POINT.z);
        renderable.worldTransform.scl(getRenderableScale());

        renderable.meshPart.center.set(0f, START_POINT.z, 0f);      // for RenderableSorter

        return renderable;
    }

    private float getRenderableScale() {

        return rand.nextFloat() * SCALE;

    }

    private float getRenderableXPosition() {

        return rand.nextFloat() * (SCENE_RIGHT - SCENE_LEFT) + SCENE_LEFT;

    }

    // Calculates texture coordinates offsets for vertex shader
    private Vector2 calcUVMapping(int num) {

        int row, column;

        column = (num / (int)UV_TEXTURE.x);                         // column num, starts from 0

        if(num > (int)UV_TEXTURE.x) {
            row = num - (column * (int)UV_TEXTURE.x);
        } else {
            row = num;
        }

        return new Vector2(1f / UV_TEXTURE.x * column, 1f / UV_TEXTURE.y * row);
    }

    // Controls
    public void start(boolean type) {

//        if(type == RAIN) {
            setupMesh(RAIN_WIDTH, RAIN_HEIGHT);
//        } else {
//            setupMesh(SNOW_WIDTH, SNOW_HEIGHT);
//        }

        running = true;
    }

    public void stop() {

        running = false;
        particles.clear();

    }

    float delta = 0;
    float time = 0;
    // Render model
    public void render() {

        if(running == true) {

            // Update
            if(particles.size < MAX_PARTICLES) {
//                if (delta > PARTICLES_INTERVAL) {
                    emit();
//                    delta = 0f;
//                }

//                delta += STEP;
            }


            Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

            spriteBatch.begin();
            spriteBatch.draw(backgroud, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
            spriteBatch.end();

            batch.begin(camera);

            Gdx.gl.glEnable(GL20.GL_BLEND);
            Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);

            for(int i = 0; i < particles.size; i++) {
                Particle p = particles.get(i);

                if(getCurrentTime() - p.startTime > p.lifeTime) {
                    time = getCurrentTime();
                    p.startTime = time;
                    p.renderable.worldTransform.translate(getRenderableXPosition(), 0f, 0f);
                    p.shader.setStartTime(time);
                }

                batch.render(p.renderable);
            }

            batch.end();

        }
    }

    public void dispose() {

        particles.clear();
        if(material != null) material.clear();
        if(texture != null) texture.dispose();
        if(program != null) program.dispose();

    }

}
