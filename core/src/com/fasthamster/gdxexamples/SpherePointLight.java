package com.fasthamster.gdxexamples;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.PointLight;
import com.badlogic.gdx.graphics.g3d.model.Node;
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.utils.Array;

/**
 * Created by alex on 15.02.16.
 */
public class SpherePointLight extends ApplicationAdapter {

    public static final String TAG = SpherePointLight.class.getName();

    private Environment environment;
    private Array<ModelInstance> instances = new Array();
    private PerspectiveCamera camera;
    private ModelBatch modelBatch;
    private ModelBuilder builder;
    private CameraInputController camController;


    @Override
    public void create () {

        builder = new ModelBuilder();
        modelBatch = new ModelBatch();

        setupCam();
        setupSphere();
        setupBox();
//        setupPlane();


        environment = new Environment();
//        environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.4f, 0.4f, 1.0f));
        environment.add(new PointLight().set(0f, 0f, 1f, 0f, 0f, -1f, 100f));

        camController = new CameraInputController(camera);
        Gdx.input.setInputProcessor(camController);

    }

    private void setupSphere() {

        Material material = new Material();
        material.set(ColorAttribute.createDiffuse(Color.BLUE));

        Model model = builder.createSphere(2f, 2f, 2f, 16, 16, material, VertexAttributes.Usage.Position | VertexAttributes.Usage.TextureCoordinates | VertexAttributes.Usage.Normal);

        instances.add(new ModelInstance(model));

    }

    private void setupBox() {

        Material material = new Material();
        material.set(ColorAttribute.createDiffuse(Color.WHITE));

        Model box = builder.createBox(8f, 8f, 1f, material, VertexAttributes.Usage.Position | VertexAttributes.Usage.TextureCoordinates | VertexAttributes.Usage.Normal);

        ModelInstance cube = new ModelInstance(box);

        cube.transform.setToTranslation(0f, 0f, -2f);

        instances.add(cube);

    }

    private void setupPlane() {

        Material material = new Material();
        material.set(ColorAttribute.createDiffuse(Color.WHITE));

        Model rect = builder.createRect(
                4f, -4f, 0f,
                -4f, -4f, 0f,
                -4f, 4f, 0f,
                4f, 4f, 0f,
                0,
                1,
                0,
                material, VertexAttributes.Usage.Position | VertexAttributes.Usage.TextureCoordinates | VertexAttributes.Usage.TextureCoordinates);

        ModelInstance plane = new ModelInstance(rect);

        Node node = plane.nodes.get(0);
        plane.transform.set(node.globalTransform);
        node.translation.set(0f, 0f, -1f);
        node.rotation.setEulerAngles(180f, 0f, 0f);
        plane.calculateTransforms();

        instances.add(plane);

    }

    private void setupCam() {

        camera = new PerspectiveCamera(67f, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.position.set(0f, 0f, 8f);
        camera.lookAt(0f, 0f, 0f);
        camera.near = 0.1f;
        camera.far = 100f;
        camera.update();

    }

    @Override
    public void render () {

        camController.update();

        Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

//        Gdx.gl.glEnable(GL20.GL_BLEND);
//        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);


        modelBatch.begin(camera);
        modelBatch.render(instances, environment);
        modelBatch.end();

    }

    @Override
    public void dispose() {

        modelBatch.dispose();

    }
}
