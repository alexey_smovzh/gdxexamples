package com.fasthamster.gdxexamples;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.loader.G3dModelLoader;
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.UBJsonReader;

/**
 * Created by alex on 12.05.16.
 */
public class FlatLight extends ApplicationAdapter {

    public static final String TAG = FlatLight.class.getName();

    private ModelInstance volcano;
    private ModelBatch batch;
    private PerspectiveCamera camera;

    private FlatLightShaderProvider shaderProvider;

    private CameraInputController controller;

    private boolean paused;



    @Override
    public void create () {

        // Model batch with shader provider
        shaderProvider = new FlatLightShaderProvider();
        batch = new ModelBatch(shaderProvider);

        setupCam();
        setupModels(batch);

        paused = false;

    }

    private void setupCam() {

        camera = new PerspectiveCamera(50f, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        if((float)Gdx.graphics.getHeight() / (float)Gdx.graphics.getWidth() > 1f) {   // Portrait
            camera.position.set(10f, 3f, -11f);
            camera.lookAt(0f, 0f, 0f);
        } else {                                                                      // Landscape
            camera.position.set(18f, 8f, -18f);
            camera.lookAt(0f, 6f, 0f);
        }

        camera.near = 0.1f;
        camera.far = 100f;
        camera.update();

        controller = new CameraInputController(camera);
        Gdx.input.setInputProcessor(controller);

    }

    private void setupModels(ModelBatch batch) {

        Array<ModelInstance> particles = new Array<ModelInstance>();

        UBJsonReader jsonReader = new UBJsonReader();
        G3dModelLoader modelLoader = new G3dModelLoader(jsonReader);

        Model model = modelLoader.loadModel(Gdx.files.internal("volcano.g3db"));
        volcano = new ModelInstance(model);

        for(Material m : volcano.materials)
            m.set(new gdxExamplesBooleanAttribute(gdxExamplesBooleanAttribute.FlatLight, true));

    }

    @Override
    public void render () {

        if(!paused) {

            controller.update();

            // Render cycle
            Gdx.gl.glClearColor(0.5f, 0.6f, 0.7f, 1.0f);
            Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);


            batch.begin(camera);
            batch.render(volcano);
            batch.end();

        }
    }

    @Override
    public void pause() {

        paused = true;

    }

    @Override
    public void resume() {

        paused = false;

    }

    @Override
    public void dispose() {

        batch.dispose();

    }
}