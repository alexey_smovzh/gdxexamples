package com.fasthamster.gdxexamples;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g3d.Attributes;
import com.badlogic.gdx.graphics.g3d.Renderable;
import com.badlogic.gdx.graphics.g3d.Shader;
import com.badlogic.gdx.graphics.g3d.shaders.BaseShader;
import com.badlogic.gdx.graphics.g3d.shaders.DefaultShader;
import com.badlogic.gdx.graphics.g3d.utils.RenderContext;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;

/**
 * Created by alex on 23.07.16.
 */
public class LowPolyBonfireShader extends BaseShader {

    private final int u_projViewTrans = register(new Uniform("u_projViewTrans"), DefaultShader.Setters.projViewTrans);
    private final int u_worldTrans = register(new Uniform("u_worldTrans"), DefaultShader.Setters.worldTrans);
    private final int u_normalMatrix = register(new Uniform("u_normalMatrix"), DefaultShader.Setters.normalMatrix);
    private final int u_diffuseColor = register(new Uniform("u_diffuseColor"), DefaultShader.Setters.diffuseColor);
    private final int u_time = register(new Uniform("u_time"));
    private final int u_resolution = register(new Uniform("u_resolution"));

    private Renderable renderable;


    // Constructor
    public LowPolyBonfireShader(final Renderable renderable) {

        String light = Gdx.files.internal("phong.vert").readString();

        String vert = Gdx.files.internal("LowPolyBonfire.vert").readString();
        String frag = Gdx.files.internal("LowPolyBonfire.frag").readString();

        this.program = new ShaderProgram(light + vert, frag);
//        this.program = new ShaderProgram(vert, frag);
        this.renderable = renderable;

    }

    @Override
    public void init() {

        final ShaderProgram program = this.program;
        this.program = null;
        init(program, renderable);
        renderable = null;

    }

    @Override
    public void begin(final Camera camera, final RenderContext context) {

        super.begin(camera, context);
        set(u_time, (System.nanoTime() - LowPolySky.globalStartTime) / 1000000000f);
        set(u_resolution, camera.viewportHeight, camera.viewportWidth);

        context.setCullFace(GL20.GL_BACK);
        context.setDepthTest(GL20.GL_LEQUAL);

    }

    @Override
    public void render(Renderable renderable, Attributes combinedAttributes) {

        super.render(renderable, combinedAttributes);

    }

    @Override
    public void end() {

        super.end();

    }

    @Override
    public int compareTo(Shader other) {
        return 0;
    }

    @Override
    public boolean canRender(Renderable renderable) {

        return true;

    }
}
