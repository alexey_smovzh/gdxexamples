package com.fasthamster.gdxexamples;


import com.badlogic.gdx.graphics.g3d.Attribute;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by alex on 20.01.16.
 */
public class gdxExamplesVector2Attribute extends Attribute {

    public final static String UVTextureAlias = "UVTexture";
    public final static String UVCoordAlias = "UVCoord";

    public final static long UVTexture = register(UVTextureAlias);
    public final static long UVCoord = register(UVCoordAlias);

    protected static long UVTextureMask = UVTexture | UVCoord;

    public Vector2 value;


    public gdxExamplesVector2Attribute(final long type, final Vector2 value) {
        super(type);
        this.value = value;
    }

    // COnstructor for copy() method
    public gdxExamplesVector2Attribute(gdxExamplesVector2Attribute other) {
        this(other.type, other.value);
    }

    @Override
    public Attribute copy() { return new gdxExamplesVector2Attribute(this); }

    @Override
    public boolean equals(Attribute other) {
        return ((gdxExamplesVector2Attribute)other).value == value;
    }

    @Override
    public int compareTo(Attribute o) {
        if(type != o.type) return type < o.type ? -1 : 1;
        Vector2 otherValue = ((gdxExamplesVector2Attribute)o).value;
        return value.equals(otherValue) ? 0 : (value.len() < otherValue.len() ? -1 : 1);
    }
}


