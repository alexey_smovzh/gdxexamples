package com.fasthamster.gdxexamples;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by alex on 16.12.16.
 */

public class ReloadTextureToShader extends ApplicationAdapter {

    public static final String TAG = ReloadTextureToShader.class.getName();

    private Matrix4 projection = new Matrix4();
    private ShaderProgram shader;
    private Mesh mesh;
    private Triangulator triangulator0;
    private Triangulator triangulator1;
    private Runnable runnable0;
    private Runnable runnable1;

    private Vector2 resolution = new Vector2();
    private boolean paused;



    @Override
    public void create () {

        Gdx.app.setLogLevel(Application.LOG_DEBUG);

        setupTriangulators();
        setupRunnables();

        // Load shader
        shader = new ShaderProgram(Gdx.files.internal("simple.vert").readString(),
                                   Gdx.files.internal("simple.frag").readString());

        if (shader.getLog().length() != 0)
            Gdx.app.log(TAG, shader.getLog());

        paused = false;

    }

    int clock = 0;
    float factor = 0f;
    float increment = 0.01f;
    @Override
    public void render () {

        Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);

        if(!paused) {

            clock ++;

            if(clock > 4000) {

                factor += increment;

                if (factor >= 1f) {
                    Gdx.app.log(TAG, "factor = 1");
                    clock = 0;
                    factor = 1f;
                    increment = -0.01f;
                    new Thread(runnable0).start();
                }

                if (factor <= 0f) {
                    Gdx.app.log(TAG, "factor = 0");
                    clock = 0;
                    factor = 0f;
                    increment = 0.01f;
                    new Thread(runnable1).start();
                }
            }

            shader.begin();
            shader.setUniformMatrix("u_projTrans", projection);
            shader.setUniformi("u_normal1", 1);
            shader.setUniformi("u_normal0", 0);
            shader.setUniformf("u_factor", factor);

            mesh.render(shader, GL20.GL_TRIANGLES);

            shader.end();

        }
    }

    @Override
    public void dispose () {

        if(shader != null) shader.dispose();
        if(mesh != null) mesh.dispose();
        if(triangulator0 != null) triangulator0.dispose();
        if(triangulator1 != null) triangulator1.dispose();

    }

    @Override
    public void resize(int w, int h) {

        if(resolution.x != (float)w) {
            resolution.set((float) w, (float) h);

            if (mesh != null) mesh.dispose();
            mesh = setupMesh();

            projection.setToOrtho2D(0f, 0f, resolution.x, resolution.y);

            triangulator0.setResolution(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
            triangulator1.setResolution(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
            triangulator0.setCellSize(80);
            triangulator1.setCellSize(80);
            triangulator0.setProjection(projection);
            triangulator1.setProjection(projection);
            triangulator0.bindNormalMap(0);
            triangulator0.bindNormalMap(1);

        }
    }

    @Override
    public void pause() {
        paused = true;
    }

    @Override
    public void resume() {

        paused = false;
    }

    private void setupTriangulators() {

        triangulator0 = new Triangulator();
        triangulator1 = new Triangulator();

    }

    private void setupRunnables() {

        runnable0 = new Runnable() {
            @Override
            public void run() {
                triangulator0.triangulate();
                Gdx.app.postRunnable(new Runnable() {
                    @Override
                    public void run() {
                        triangulator0.render(0);
                    }
                });
            }
        };

        runnable1 = new Runnable() {
            @Override
            public void run() {
                triangulator1.triangulate();
                Gdx.app.postRunnable(new Runnable() {
                    @Override
                    public void run() {
                        triangulator1.render(1);
                    }
                });
            }
        };
    }

    // Generate mesh
    private Mesh setupMesh() {

        Mesh mesh = new Mesh(true,
                4, 6,
                new VertexAttribute(VertexAttributes.Usage.Position, 3, "a_position"),
                new VertexAttribute(VertexAttributes.Usage.TextureCoordinates, 2, "a_texCoord0"));

        mesh.setVertices(new float[]{           // Vertices + UVs
                resolution.x, 0f, 0f, 0f, 1f,
                0f, 0f, 0f, 1f, 1f,
                0f, resolution.y, 0f, 1f, 0f,
                resolution.x, resolution.y, 0f, 0f, 0f});

        mesh.setIndices(new short[]{0, 1, 2, 2, 3, 0});

        return mesh;

    }
}
