package com.fasthamster.gdxexamples;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.DelaunayTriangulator;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.FloatArray;
import com.badlogic.gdx.utils.ShortArray;

/**
 * Created by alex on 27.08.16.
 */

// From https://github.com/libgdx/libgdx/blob/master/tests/gdx-tests/src/com/badlogic/gdx/tests/DelaunayTriangulatorTest.java
public class DelaunayTriangulatorTest extends InputAdapter implements ApplicationListener {

    private ShapeRenderer renderer;
    FloatArray points = new FloatArray();
    ShortArray triangles;
    DelaunayTriangulator trianglulator = new DelaunayTriangulator();

    private static final int SIDE = 1024;
    private static final int CELL_SIZE = 40;

    private static final Color[] colors = {

            new Color(0.337f, 0.353f, 0.969f, 1.000f),
            new Color(0.357f, 0.376f, 0.961f, 1.000f),
            new Color(0.443f, 0.376f, 0.988f, 1.000f),
            new Color(0.569f, 0.345f, 0.980f, 1.000f),
            new Color(0.420f, 0.404f, 0.980f, 1.000f),
            new Color(0.514f, 0.376f, 0.992f, 1.000f),
            new Color(0.373f, 0.431f, 0.984f, 1.000f),
            new Color(0.475f, 0.404f, 0.988f, 1.000f),
            new Color(0.412f, 0.427f, 0.996f, 1.000f),
            new Color(0.514f, 0.400f, 1.000f, 1.000f),
            new Color(0.455f, 0.420f, 1.000f, 1.000f),
            new Color(0.333f, 0.467f, 0.980f, 1.000f),
            new Color(0.573f, 0.396f, 0.988f, 1.000f),
            new Color(0.388f, 0.455f, 0.984f, 1.000f),
            new Color(0.553f, 0.412f, 0.992f, 1.000f),
            new Color(0.502f, 0.435f, 0.996f, 1.000f),
            new Color(0.475f, 0.447f, 0.996f, 1.000f),
            new Color(0.447f, 0.459f, 1.000f, 1.000f),
            new Color(0.424f, 0.475f, 1.000f, 1.000f),
            new Color(0.604f, 0.439f, 0.992f, 1.000f),
            new Color(0.427f, 0.494f, 0.973f, 1.000f),
            new Color(0.373f, 0.510f, 0.996f, 1.000f),
            new Color(0.400f, 0.502f, 1.000f, 1.000f),
            new Color(0.435f, 0.494f, 1.000f, 1.000f),
            new Color(0.557f, 0.459f, 1.000f, 1.000f),
            new Color(0.502f, 0.478f, 1.000f, 1.000f),
            new Color(0.529f, 0.471f, 1.000f, 1.000f),
            new Color(0.475f, 0.490f, 1.000f, 1.000f),
            new Color(0.596f, 0.471f, 0.996f, 1.000f),
            new Color(0.573f, 0.482f, 1.000f, 1.000f),
            new Color(0.545f, 0.494f, 1.000f, 1.000f),
            new Color(0.624f, 0.475f, 0.984f, 1.000f),
            new Color(0.490f, 0.514f, 1.000f, 1.000f),
            new Color(0.365f, 0.553f, 0.984f, 1.000f),
            new Color(0.522f, 0.506f, 1.000f, 1.000f),
            new Color(0.671f, 0.467f, 0.965f, 1.000f),
            new Color(0.463f, 0.525f, 1.000f, 1.000f),
            new Color(0.498f, 0.529f, 0.992f, 1.000f),
            new Color(0.584f, 0.506f, 0.996f, 1.000f),
            new Color(0.624f, 0.498f, 0.984f, 1.000f),
            new Color(0.561f, 0.518f, 0.996f, 1.000f),
            new Color(0.451f, 0.557f, 1.000f, 1.000f),
            new Color(0.478f, 0.549f, 1.000f, 1.000f),
            new Color(0.420f, 0.580f, 0.988f, 1.000f),
            new Color(0.600f, 0.529f, 0.992f, 1.000f),
            new Color(0.647f, 0.525f, 0.973f, 1.000f),
            new Color(0.522f, 0.561f, 0.996f, 1.000f),
            new Color(0.576f, 0.545f, 0.996f, 1.000f),
            new Color(0.553f, 0.557f, 1.000f, 1.000f),
            new Color(0.584f, 0.561f, 0.984f, 1.000f),
            new Color(0.639f, 0.545f, 0.988f, 1.000f),
            new Color(0.682f, 0.537f, 0.969f, 1.000f),
            new Color(0.616f, 0.557f, 0.992f, 1.000f),
            new Color(0.455f, 0.608f, 0.984f, 1.000f),
            new Color(0.533f, 0.584f, 0.992f, 1.000f),
            new Color(0.486f, 0.604f, 0.992f, 1.000f),
            new Color(0.608f, 0.576f, 0.976f, 1.000f),
            new Color(0.424f, 0.643f, 0.992f, 1.000f),
            new Color(0.565f, 0.604f, 0.988f, 1.000f),
            new Color(0.600f, 0.612f, 0.976f, 1.000f),
            new Color(0.686f, 0.596f, 0.961f, 1.000f),
            new Color(0.549f, 0.655f, 0.976f, 1.000f),
            new Color(0.624f, 0.635f, 0.976f, 1.000f),
            new Color(0.627f, 0.651f, 0.965f, 1.000f),

    };


    public void create () {

        renderer = new ShapeRenderer();

        triangulate();

        Gdx.graphics.setContinuousRendering(false);         // Render once
        Gdx.graphics.requestRendering();

    }

    // Generate point array with random points positions
    void triangulate () {

        for(int horizontal = -CELL_SIZE; horizontal < SIDE; horizontal += CELL_SIZE) {
            for (int vertical = -CELL_SIZE; vertical < SIDE; vertical += CELL_SIZE) {

                points.add(MathUtils.random(horizontal, horizontal + CELL_SIZE));
                points.add(MathUtils.random(vertical, vertical + CELL_SIZE));

            }
        }

        triangles = trianglulator.computeTriangles(points, false);

    }

    public void render () {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        renderer.begin(ShapeRenderer.ShapeType.Filled);

        for (int i = 0; i < triangles.size; i += 3) {
            int p1 = triangles.get(i) * 2;
            int p2 = triangles.get(i + 1) * 2;
            int p3 = triangles.get(i + 2) * 2;

            renderer.setColor(colors[MathUtils.random(colors.length-1)]);
            renderer.triangle( //
                    points.get(p1), points.get(p1 + 1), //
                    points.get(p2), points.get(p2 + 1), //
                    points.get(p3), points.get(p3 + 1));
        }

        renderer.end();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }

    public void resize (int width, int height) {

        renderer.getProjectionMatrix().setToOrtho2D(0, 0, width, height);
        renderer.updateMatrices();

    }
}